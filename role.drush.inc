<?php

/**
 * Implementation of hook_drush_command().
 */
function role_drush_command() {
  $items['role-add-perm'] = array(
    'description' => '',
    'arguments' => array(
      'role' => 'role',
      'perm' => 'perm',
    ),
    'options' => array(
      'module' => 'List the permissions provided by a specific module.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_MAX,
  );

  $items['role-remove-perm'] = array(
    'description' => '',
    'arguments' => array(
      'role' => 'role',
      'perm' => 'perm',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_MAX,
  );

  $items['role-list'] = array(
    'description' => 'Display a list of roles.',
    'aliases' => array('rls'),
    'options' => array(
      'perm' => 'Perform a search on roles that have a certain permission',
    ),
    'examples' => array(
      "drush role-list --perm='administer nodes'" => 'Display a list of roles that have the administer nodes permission assigned.',
    ),
  );

  return $items;
}


function drush_role_add_perm($role, $permission = NULL) {
  $permissions = array();
  $roles = user_roles();

  // If a permission wasn't provided, but the module option is specified,
  // provide a list of permissions provided by that module.
  if (!$permission && $module = drush_get_option('module', FALSE)) {
    $module_perms = module_invoke($module, 'perm');
    $choice = drush_choice($module_perms, 'Enter a number to choose which permission to add.');
    if ($choice !== FALSE) {
      $permission = $module_perms[$choice];
    }
  }
  else {
    foreach (module_list(FALSE, FALSE, TRUE) as $module) {
      if ($perms = module_invoke($module, 'perm')) {
        $permissions = array_merge($permissions, $perms);
      }
    }
    if (!in_array($permission, $permissions)) {
      drush_set_error(dt('Could not find the permission: !perm', array('!perm' => $permission)));
    }
  }

  if (!is_numeric($role) && in_array($role, $roles)) {
    $role = array_search($role, $roles);
  }
  elseif (!isset($roles[$role])) {
    drush_set_error(dt('Could not find the role: !role', array('!role' => $role)));
  }

  $role_perms = db_result(db_query("SELECT perm FROM {permission} pm LEFT JOIN {role} r ON r.rid = pm.rid WHERE r.rid = '%d'", $role));
  $role_perms = explode(", ", $role_perms);
  $role_perms = array_filter($role_perms);

  if (!in_array($permission, $role_perms)) {
    $role_perms[] = $permission;

    $new_perms = implode(", ", $role_perms);
    db_query("UPDATE {permission} LEFT JOIN {role} ON role.rid = permission.rid SET perm = '%s' WHERE role.rid= '%d'", $new_perms, $role);
    drush_print(dt('Added "!perm" to "!role"', array('!perm' => $permission, '!role' => $roles[$role])));
  }
  else {
    drush_print(dt('"!role" already has the permission "!perm"', array('!perm' => $permission, '!role' => $roles[$role])));
  }
  cache_clear_all();
}

/**
 * Displays a list of roles
 */
function drush_role_list() {
  // get options passed
  $perm = drush_get_option('perm');
  $roles = array();

  // get all roles - if $perm is empty user_roles retrieves all roles
  $roles = user_roles(FALSE, $perm);

  if (!empty($roles)) {
    $rows[] = array('rid',dt('Name'));
    foreach($roles as $rid => $name) {
      $rows[] = array($rid, $name);
      drush_print_pipe($rid .','. $name ."\n");
    }
    $num = count($roles);

    drush_print_table($rows, TRUE);
  }

  else {
    drush_set_error('No roles found.');
  }
}
